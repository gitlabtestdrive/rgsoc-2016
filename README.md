# Rails Girls Summer of Code 2016

![Rails Girls Summer of Code Logo](logo.png)

This repository is used for managing the RGSoC 2016 including tasks such as (but
not limited to) making sure students have everything set up and discussing
topics too large for chat channels. Most of the party takes place at the issue
tracker.

## Contact Details

| Name             | Role    | Email                   | GitLab.com     | Slack          | GitHub         |
|:-----------------|:--------|:------------------------|:---------------|:---------------|:---------------|
| Yorick Peterse   | Mentor  | <yorick@gitlab.com>     | @yorickpeterse | @yorickpeterse | @YorickPeterse |
| Heather McNamee  | Coach   | <heather@gitlab.com>    | @nearlythere   | @heather       | @nearlythere   |
| Grzegorz Bizon   | Coach   | <grzegorz@gitlab.com>   | @grzesiek      | @grzegorz      | @grzesiek      |
| Tomasz Maczukin  | Coach   | <tomasz@gitlab.com>     | @tmaczukin     | @tmaczukin     | @tmaczukin     |
| Piotr Szotkowski | Coach   | <chastell@chastell.net> | ...            | ...            | @chastell      |
| Ula              | Student |                         | @ubudzisz      | @ula           | @ubudzisz      |
| Kasia            | Student |                         | @kradydal      | @kasia         | @kradydal      |
